package com.nxctophile.rhythmie

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material.icons.rounded.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import com.nxctophile.rhythmie.data.BottomNavigation

val items = listOf(
    BottomNavigation(
        title = "Home",
        icon = Icons.Rounded.Home,
    ),
    BottomNavigation(
        title = "Explore",
        icon = Icons.Rounded.Search,
    ),
    BottomNavigation(
        title = "Library",
        icon = Icons.Rounded.Menu,
    ),
)
val ralewayFont = FontFamily(
    Font(R.font.raleway)
)
@Preview
@Composable
fun BottomNavigationBar() {
    NavigationBar {
        Row(
            modifier = Modifier.background(Color.Black)
        ) {
            items.forEachIndexed { index, item -> 
                NavigationBarItem(selected = index == 0,
                    onClick = {},
                    icon = {
                        Icon(imageVector = item.icon, contentDescription = item.title, tint = MaterialTheme.colorScheme.onBackground)
                    },
                    label = {
                        Text(text = item.title, color = MaterialTheme.colorScheme.onBackground, fontFamily = ralewayFont)
                    }
                )
            }
        }
    }
}